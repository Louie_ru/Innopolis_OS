#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <wait.h>

int fd[2];

int  main(void) {
    int child_id2;
    pipe(fd);
    if (fork() == 0){
        printf("child 1 started\n");
        read(fd[0], &child_id2, sizeof(child_id2));
        printf("child 1 received pid of child 2: %d\n", child_id2);
        sleep(4);
        printf("child 1 send SIGSTOP to child 2\n");
        kill(child_id2, SIGSTOP);
    }
    else{
        if ((child_id2 = fork()) == 0){
            printf("child 2 started\n");
            while(1){
                printf("child 2 is alive\n");
                sleep(1);
            }
        }
        else{
            printf("main send pid of child 2: %d\n", child_id2);
            write(fd[1], &child_id2, sizeof(child_id2));
            printf("main waits for change in child 2\n");
            int status;
            waitpid(child_id2, &status, WUNTRACED);
            printf("main saw child2 is stopped\n");
        }
    }
}

/*
main send pid of child 2: 22540
main waits for change in child 2
child 1 started
child 2 started
child 2 is alive
child 1 received pid of child 2: 22540
child 2 is alive
child 2 is alive
child 2 is alive
child 1 send SIGSTOP to child 2
main saw change in child2
*/