#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

void SIGKILL_handler(int sig) {
    signal(SIGKILL, SIGKILL_handler);
    printf("SIGKILL\n");
}

void SIGSTOP_handler(int sig) {
    signal(SIGSTOP, SIGSTOP_handler);
    printf("SIGSTOP\n");
}

void SIGUSR1_handler(int sig) {
    signal(SIGUSR1, SIGUSR1_handler);
    printf("SIGUSR1\n");
}

int  main(void) {
    signal(SIGKILL, SIGKILL_handler);
    signal(SIGSTOP, SIGSTOP_handler);
    signal(SIGUSR1, SIGUSR1_handler);
    while (1) pause();
}

/*
louie@LAPTOP:$ ./a.out&
[1] 19719                              //Process is started
louie@LAPTOP:$ kill -SIGUSR1 19719
SIGUSR1                                //Signal handled
louie@LAPTOP:$ ./a.out& kill -SIGSTOP 19719
[1]+  Остановлен    ./a.out
louie@LAPTOP:$ ./a.out& kill -SIGKILL 19719
[1]-  Убито              ./a.out
*/
