#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>

int fd[2];
char* a = "hello";
char buff[100];

int main() {
    pipe(fd);
    if(fork() == 0){
        read(fd[0], buff, sizeof(buff));
        printf("Transferred: %s", buff);
        exit(0);
    }
    else{
        write(fd[1], a, (strlen(a)+1));
    }
    return 0;
}