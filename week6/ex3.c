#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

void SIGINT_handler(int sig) {
    signal(sig, SIGINT_handler);
    printf("catched SIGINT");
    getchar();
}

int  main(void) {
    signal(SIGINT, SIGINT_handler);
    while (1) pause();
}
