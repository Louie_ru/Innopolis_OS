#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

int fd[2];
char* a = "hello";
char buff[100];

int main() {
    pipe(fd);
    write(fd[1], a, (strlen(a)+1));
    read(fd[0], buff, sizeof(buff));
    printf("Transferred: %s", buff);
    return 0;
}
