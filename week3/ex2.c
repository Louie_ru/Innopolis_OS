#include <stdio.h>

void swap_ints(int *a, int *b){
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void bubble_sort(int *arr, int len){
    for (int i = 0; i < len - 1; i++){
        for (int j = 0; j < len - 1 - i; j++){
            if (*(arr+j) > *(arr+j+1)){
                swap_ints(arr+j, arr+j+1);
            }
        }
    }
}

int main(){
    int a[] = {0, 1, 9, 2, 8, 3, 7, 4, 6, 5};
    int len = 10;
    bubble_sort(a, len);
    for (int i = 0; i < len; i++)
        printf("%d ", a[i]);
    return 0;
}
