#include <stdio.h>
#include <stdbool.h>

#define SIZE 10

int arr[SIZE] = {0,1,9,2,8,3,7,4,6,5};

void swap_by_indexes(int a, int b) {
    int temp = arr[a];
    arr[a] = arr[b];
    arr[b] = temp;
}

int partition(int l, int r, int piv) {
    int lp = l - 1, rp = r;
    while(1) {
        while(1){
            lp++;
            if (arr[lp] < piv) continue;
            else break;
        }
        while(1){
            rp--;
            if (rp <= 0 || arr[rp] <= piv) break;
        }
        if(lp >= rp) break;
        else swap_by_indexes(lp,rp);
    }
    swap_by_indexes(lp, r);
    return lp;
}

void q_sort(int l, int r) {
    if(r - l <= 0)
        return;
    int pPosition = partition(l, r, arr[r]);
    q_sort(l,pPosition - 1);
    q_sort(pPosition + 1,r);
}

int main() {
    q_sort(0, SIZE-1);
    for(int i = 0; i < SIZE; i++)
        printf("%d ", arr[i]);
    printf("\n");
    return 0;
}
