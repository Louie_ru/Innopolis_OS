#include <stdio.h>
#include <stdlib.h>

typedef struct node{
      int val;
      struct node *next;
      struct node *prev;
};

struct node* node_new(struct node* pr, struct node* ne, int va) {
  struct node* p = malloc(sizeof(struct node));
  p->prev = pr;
  p->next = ne;
  p->val = va;
  return p;
}


void insert_to(struct node* head, int pos, int val){
    struct node *cur = head;
    for (int i = 0; i < pos; i++)
        cur = cur->next;
    struct node *new_node = node_new(cur, cur->next, val);
    if (cur->next != NULL){
        free(cur->next);
        cur->next = node_new(new_node, cur->next->next, cur->next->val);
    }
    cur->next = new_node;
}

void delete_from(struct node* head, int pos){
    struct node *cur = head;
    for (int i = 0; i < pos; i++)
        cur = cur->next;

    if (cur->prev != NULL)
        cur->prev->next = cur->next;
    if (cur->next != NULL)
        cur->next->prev = cur->prev;
    free(cur);
}

void print_list(struct node* head){

    for (struct node* cur = head;; cur = cur->next){
        printf("%d ", cur->val);
        if (cur->next == NULL) break;
    }
}


int main(){
    struct node *head = node_new(NULL, NULL, 1);
    insert_to(head, 0, 2);
    insert_to(head, 1, 3);
    insert_to(head, 2, 4);
    insert_to(head, 3, 5);
    delete_from(head, 2);
    print_list(head);
    return 0;
}
