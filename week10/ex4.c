#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>

struct node{
    int node;
    int count;
    char names[10][512];
};

int main(){
    int count;
    struct DIR *d;
    struct dirent* dp;
    struct node nodes[1000];
    int next_node_ind = 0;

    if((d = opendir("tmp/")) != NULL){
        while ((dp = readdir(d)) != NULL) {
            int inode = dp->d_ino;
            int found = 0;
            for (int i = 0; i < next_node_ind; i++){
                if (nodes[i].node == inode){
                    strcpy(nodes[i].names[nodes[i].count], dp->d_name);
                    nodes[i].count++;
                    found = 1;
                    break;
                }
            }
            if (found == 0){
                nodes[next_node_ind].node = inode;
                strcpy(nodes[next_node_ind].names[0], dp->d_name);
                nodes[next_node_ind].count = 1;
                next_node_ind++;
            }
        }
        closedir(d);
    }

    for (int i = 0; i < next_node_ind; i++){
        if (nodes[i].count >= 2){
            for (int j = 0; j < nodes[i].count; j++)
                printf("%s ", nodes[i].names[j]);
            printf("\n");
        }
    }
    return 0;
}
