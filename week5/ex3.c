#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <stdbool.h>

#define MAX 3

bool sleep_write = 0, sleep_read = 0;
int buff[MAX];
int current = 0, incrementor = 0;

void log(char* s){
    char *cmd = malloc(strlen(s) + 16);
    strcpy(cmd, "echo '");
    strcat(cmd, s);
    strcat(cmd, "' >> file");
    system(cmd);
}

void *write(){
    log("write started");
    while(1){
        if (sleep_write){
            sleep(1);
            continue;
        }
        if (current == MAX){
            sleep_write = 1;
            return;
        }
        char str[10];
        sprintf(str, "%d", current);
        log(str);
        buff[current++] = incrementor++;
        sleep_read = 0;
        sleep(0.5);
    }
}

void *read(){
    log("read started");
    while(1){
        if (sleep_read) continue;
        if (current == 0){
            sleep_read = 1;
            continue;
        }
        int got = buff[current--];
        char str[10];
        sprintf(str, "%d", got);
        log(str);
        sleep_write = 0;
    }
}

int main(){

    pthread_t read_thread;
    pthread_t write_thread;

    pthread_create(&write_thread, NULL, write(), NULL);
    pthread_create(&read_thread, NULL, read(), NULL);

    pthread_join(read_thread, NULL);
    pthread_join(write_thread, NULL);
    return 0;
}
