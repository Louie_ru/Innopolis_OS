#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

void *myThreadFun(int i){
    printf("Hi from thread number %d\n", i);
}

int main(){
    for (int i = 0; i < 10; i++){
        pthread_t thread_id;
        pthread_create(&thread_id, NULL, myThreadFun, i);
        printf("Thread start number %d started\n", i);
        pthread_join(thread_id, NULL); //FIX
    }
    return 0;
}
/*
BEFORE FIX:
Thread start number 0 started
Thread start number 1 started
Hi from thread number 1
Thread start number 2 started
Hi from thread number 0
Hi from thread number 2
Thread start number 3 started
Hi from thread number 3
Thread start number 4 started
Hi from thread number 4
Thread start number 5 started
Hi from thread number 5
Thread start number 6 started
Hi from thread number 6
Thread start number 7 started
Hi from thread number 7
Thread start number 8 started
Hi from thread number 8
Thread start number 9 started


AFTER FIX:
Thread start number 0 started
Hi from thread number 0
Thread start number 1 started
Hi from thread number 1
Thread start number 2 started
Hi from thread number 2
Thread start number 3 started
Hi from thread number 3
Thread start number 4 started
Hi from thread number 4
Thread start number 5 started
Hi from thread number 5
Thread start number 6 started
Hi from thread number 6
Thread start number 7 started
Hi from thread number 7
Thread start number 8 started
Hi from thread number 8
Thread start number 9 started
Hi from thread number 9
*/
