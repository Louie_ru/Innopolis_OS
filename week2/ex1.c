#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(){
  int i = INT_MAX;
  float f = FLT_MAX;
  double d = DBL_MAX;
  printf("int: %d size: %d\n", i, sizeof(i));
  printf("float: %f size: %d\n", f, sizeof(f));
  printf("double: %f size: %d\n", d, sizeof(d));
  return 0;
}
