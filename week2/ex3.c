#include <stdio.h>

void print_figure1(int n){
  for (int i = 0; i < n; i++){
      for (int j = 0; j < n - i + 1; j++)
        printf(" ");
      for (int j = 0; j < 2 * i + 1; j++)
        printf("*");
      for (int j = 0; j < n - i + 1; j++)
        printf(" ");
      printf("\n");
  }
}

void print_figure2(int n){
  for (int i = 0; i < n; i++){
      for (int j = 0; j < n; j++)
        printf("*");
      printf("\n");
  }
}

void print_figure3(int n){
  for (int i = 0; i < n; i++){
      for (int j = 0; j < i - n - 1; j++)
        printf(" ");
      for (int j = 2 * i + 1; j >= 1; j--)
          printf("*");
      for (int j = 0; j < i - n - 1; j++)
        printf(" ");
      printf("\n");
  }
}

void print_figure4(int n){
  for (int i = 0; i < n; i++){
      for (int j = 0; j < n - i; j++)
        printf("*");
      for (int j = 2 * i + 1; j >= 1; j--)
          printf(" ");
      printf("\n");
  }
}

int main(){
  int m;
  printf("Enter figure number (1-4): ");
  scanf("%d", &m);

  int n;
  printf("Enter size: ");
  scanf("%d", &n);

  if (m == 1) print_figure1(n);
  else if (m == 2) print_figure2(n);
  else if (m == 3) print_figure3(n);
  else if (m == 4) print_figure4(n);

  return 0;
}
