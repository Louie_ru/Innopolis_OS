#include <stdio.h>

void swap_ints(int *i1, int *i2){
  int tmp = *i1;
  *i1 = *i2;
  *i2 = tmp;
}

int main(){
  printf("Input two integers separated by space: ");
  int a, b;
  scanf("%d %d", &a, &b);
  printf("before: %d %d\n", a, b);
  swap_ints(&a, &b);
  printf("after: %d %d\n", a, b);
  return 0;
}
