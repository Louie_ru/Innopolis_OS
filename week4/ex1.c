#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
main(){
    int n;
    pid_t pid = fork();
    printf("Hello from %s: [%d - %d]\n", pid == 0 ? "child" : "parent" ,getpid(), n);
}
