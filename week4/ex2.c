#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(){
    for (int i = 0; i < 5; i++){
            fork();
            sleep(5);
    }
}

/*
terminator─────ex2─┬─ex2─┬─ex2─┬─ex2─┬─ex2───ex2
                   │     │     │     └─ex2
                   │     │     ├─ex2───ex2
                   │     │     └─ex2
                   │     ├─ex2─┬─ex2───ex2
                   │     │     └─ex2
                   │     ├─ex2───ex2
                   │     └─ex2
                   ├─ex2─┬─ex2─┬─ex2───ex2
                   │     │     └─ex2
                   │     ├─ex2───ex2
                   │     └─ex2
                   ├─ex2─┬─ex2───ex2
                   │     └─ex2
                   ├─ex2───ex2
                   └─ex2
Terminator is my terminal, where I start ex2. Each process make forks every 5 secs until main parent terminates
*/
