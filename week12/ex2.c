#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>

const int rwx = 777;
int main(int argc, char **argv){
    char *files[100]; // usual files
    int files_count = 0;
    char *append_files[100]; //files i need to append data
    int append_files_count = 0;
    argc--; //remove first parameter (current executable)
    argv++;
    while (argc){ // while there are more parameters
        if (0 == strcmp(argv[0], "-a")){ //will append
            argv++;
            argc--;
            append_files[append_files_count] = argv[0];
            argv++;
            argc--;
            files_count++;
        }
        else{ //no append
            files[files_count] = argv[0];
            files_count++;
            argv++;
            argc--;
        }
    }
    int files_buf[200];
    int files_buf_p = 1;
    files_buf[0] = open("/dev/stdout", O_WRONLY); //always print to console
    int stdin = open("/dev/stdin", O_RDONLY);
    for (int i = 0; i < files_count; i++){//open usual files
        files_buf[files_buf_p] = open(files[i], O_CREAT | O_WRONLY, rwx);
        files_buf_p++;
    }
    for (int i = 0; i < append_files_count; i++){//open append files
        files_buf[files_buf_p] = open(append_files[i], O_WRONLY | O_APPEND | O_CREAT, rwx);
        files_buf_p++;
    }
    int bytes_num = 0;
    int buffer[1024];
    while ((bytes_num = read(stdin, buffer, sizeof(buffer))))
        for (int i = 0; i < files_buf_p; i++)
            write(files_buf[i], buffer, bytes_num);
    return 0;
}
