#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

void* my_realloc(void* arr, int sz){
    if (sz == 0){
        free(arr);
        return NULL;
    }
    int* new_arr = malloc(sz * sizeof(int));
    if (arr == NULL){
        free(arr);
        return new_arr;
    }
    memcpy(new_arr, arr, sz * sizeof(int));
    free(arr);
    return new_arr;
}

int main(){
    int* a = my_realloc(NULL, 4);
    for (int i = 0; i < 4; i++)
        a[i] = i;
    a = my_realloc(a, 6);
    for (int i = 0; i < 6; i++)
        printf("%d ", a[i]);
    //0 1 2 3 0 0
    return 0;
}
