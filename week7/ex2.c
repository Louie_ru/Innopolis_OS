#include <stdio.h>
#include <stdlib.h>

int main(){
    int sz, *ptr;
    printf("Enter zie of array: ");
    scanf("%d", &sz);
    ptr = (int*) malloc(sz * sizeof(int));
    for(int i = 0; i < sz; i++)
        ptr[i] = i;
    for(int i = 0; i < sz; i++)
        printf("%d ", ptr[i]);
    free(ptr);
    return 0;
}