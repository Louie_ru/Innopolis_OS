#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/resource.h>

const int MB = 1024 * 1024;

int main() {
    struct rusage usage;
    for (int i = 0; i < 10; i++){
        void *ptr = malloc(10 * MB);
        memset(ptr, 0, 10 * MB);
        getrusage(RUSAGE_SELF, &usage);
        printf("%ld\n", usage.ru_maxrss);
        sleep(1);
    }
    return 0;
}

/*
11060
21592
31888
42184
52480
62776
72808
83104
93400
103696
 */