#include <sys/mman.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>

int main() {
    char * nice_day = "This is a nice day";
    int fd = open("./ex1.txt", O_RDWR);
    struct stat file_stat = {};
    if (fstat(fd, &file_stat)) return -1;
    size_t sz = file_stat.st_size;
    uint8_t * arr = (uint8_t*) malloc(sz);
    if (!arr) return -1;
    if (!(arr = mmap(arr, sz, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0))) return -1;
    int i;
    for (i = 0; nice_day[i]; i++)
        arr[i] = nice_day[i];
    for (; i < sz; i++)
        arr[i] = '_';
    if (msync(arr, sz, MS_SYNC)) return -1;
    return 0;
}
