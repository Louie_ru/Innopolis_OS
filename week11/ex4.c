#include <sys/mman.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

struct my_struct {
  size_t size;
    void* ptr;
};

int file_set(const char* name, size_t fz, struct my_struct* mp){
    int fd = open(name, O_RDWR | O_CREAT, 0666);
    if (!fz) {
        struct stat my_stat = {};
        if (fstat(fd, &my_stat)) return -1;
        fz = my_stat.st_size;
    }
    else ftruncate(fd, fz);
    uint8_t* arr;
    if (arr != mmap(NULL, fz, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) return -1;
    mp->ptr = arr;
    mp->size = fz;
    return 0;
}

int main(){
    struct my_struct * my_struct_1 = malloc(sizeof(struct my_struct));
    struct my_struct * my_struct_2 = malloc(sizeof(struct my_struct));
    if (file_set("./ex1.txt", 0, my_struct_1) || file_set("./ex1.memcpy.txt", my_struct_1->size, my_struct_2))
      return -1;
    memcpy(my_struct_2->ptr, my_struct_1->ptr, my_struct_1->size);
    if (msync(my_struct_1->ptr, my_struct_1->size, MS_SYNC) || msync(my_struct_2->ptr, my_struct_2->size, MS_SYNC))
      return -1;
    return 0;
}
